(* approx: proxy server for Debian archive files
   Copyright (C) 2024  Arnaud Rebillout <arnaudr@kali.org>
   Released under the GNU General Public License *)

open OUnit2
open List
open Printf
open Testlib

let suite = [

  "immutable_test" >:::
  map (fun (res, str) ->
    sprintf "(immutable %s)" (p_str str) >::
    (fun _ -> assert_equal res (Release.immutable str)))
    [false, "debian/dists/bookworm/main/binary-amd64/Release";
     false, "debian/dists/bookworm/main/binary-amd64/Packages";
     false, "debian/dists/bookworm/main/binary-amd64/Packages.gz";
     false, "debian/dists/bookworm/main/binary-amd64/Packages.xz";
     false, "debian/dists/bookworm/main/Contents-amd64.gz";
     false, "debian/dists/bookworm/main/dep11/CID-Index-amd64.json.gz";
     false, "debian/dists/bookworm/main/dep11/Components-amd64.yml.gz";
     false, "debian/dists/bookworm/main/dep11/icons-128x128.tar.gz";
     false, "debian/dists/bookworm/main/i18n/Translation-fr.bz2";
     true,  "debian/dists/bookworm/main/installer-amd64/20230607/images/netboot/netboot.tar.gz";
     false, "debian/dists/bookworm/main/installer-amd64/current/images/netboot/netboot.tar.gz";
     false, "debian/dists/bookworm/main/source/Sources.gz";
     false, "debian/dists/sid/main/Contents-amd64.diff/Index";
     true,  "debian/dists/sid/main/Contents-amd64.diff/2024-06-08-0205.15.gz";
     true,  "debian/pool/main/n/nano/nano_8.0-1_amd64.deb";
     true,  "debian/pool/main/n/netcfg/netcfg_1.176_amd64.udeb";
     true,  "debian/pool/main/a/approx/approx_5.10-1.dsc";
     true,  "debian/pool/main/d/dnsproxy/dnsproxy_1.16-0.1.diff.gz";
     true,  "debian/pool/main/a/approx/approx_5.12.orig.tar.gz";
     true,  "debian/pool/main/a/appstream/appstream_0.14.4-1.debian.tar.xz";
     true,  "debian/pool/main/a/apr/apr_1.7.2.orig.tar.bz2";
    ]
]
